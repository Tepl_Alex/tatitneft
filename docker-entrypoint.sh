#!/usr/bin/env bash
case "$1" in
    "help")
        echo "Please use of next parameters to start: "
        echo "  > start: Start server"
        echo "  > shell: Start sh shell"
        echo "  > help: This help"
        ;;
    "shell")
        echo "Starting shell ..."
        exec bash
        ;;
    "start")
        set -o errexit
        MANAGE_PY=$(find . -maxdepth 1 -name 'manage.py*' -type f | head -n1)

        echo "Apply migrations..."
        python "${MANAGE_PY}" migrate
        echo "Collect static files..."
        python "${MANAGE_PY}" collectstatic --noinput --clear
        echo "Run backend..."
        re='^[0-9]+$'
        if ! [[ "$2" =~ $re ]] ; then
            echo "Not a number or empty"
            TIMEOUT=30
        else
            echo "Apply new timeout"
            TIMEOUT="$2"
        fi

        exec gunicorn -b 0.0.0.0:8000 -t $TIMEOUT tatitneft.wsgi:application
        ;;
esac
