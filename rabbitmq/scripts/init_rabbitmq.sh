#!/bin/sh

PASS=test

docker-compose exec rabbitmq rabbitmq-plugins enable rabbitmq_web_stomp
docker-compose exec rabbitmq rabbitmqctl add_vhost ws
docker-compose exec rabbitmq rabbitmqctl add_vhost jobs

# Пользователь для общения
docker-compose exec rabbitmq rabbitmqctl add_user ws "ws_$PASS"
docker-compose exec rabbitmq rabbitmqctl set_permissions -p ws ws ".*" ".*" ".*"

# Пользователь для клиента (только на чтение):
docker-compose exec rabbitmq rabbitmqctl add_user ws_readonly ws_readonly
docker-compose exec rabbitmq rabbitmqctl set_permissions -p ws ws_readonly ".*" "^$" ".*"

# Пользователь для фоновых задач бэка (пароль поменять):
docker-compose exec rabbitmq rabbitmqctl add_user jobs "jobs_$PASS"
docker-compose exec rabbitmq rabbitmqctl set_permissions -p jobs jobs ".*" ".*" ".*"


