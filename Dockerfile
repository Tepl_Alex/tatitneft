# Stage 1: Сборка проекта
FROM python:3.8-slim as build

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        build-essential \
    # obtain dadata from repository
    && apt-get install -y --no-install-recommends \
        git \
    # pillow building dependencies
    && apt-get install -y --no-install-recommends \
        libtiff5-dev libjpeg-dev libopenjp2-7-dev zlib1g-dev \
        libfreetype6-dev liblcms2-dev libwebp-dev tcl8.6-dev tk8.6-dev python3-tk \
        libharfbuzz-dev libfribidi-dev \
    # for django gdal support
    && apt-get install -y --no-install-recommends \
        binutils libproj-dev gdal-bin

WORKDIR /var/www/backend

RUN pip install --no-cache-dir pipenv

COPY ./docker-entrypoint.sh ./
COPY ./Pipfile* ./
COPY ./tatitneft ./

# Установка зависимостей проекта
RUN cd /var/www/backend \
    && pipenv requirements --dev > /tmp/requirements.txt \
    && pip install --no-cache-dir -r /tmp/requirements.txt \
    && rm /tmp/requirements.txt

ENTRYPOINT ["/var/www/backend/docker-entrypoint.sh"]
CMD ["help"]
