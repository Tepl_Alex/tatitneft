from django import get_version
from .celery import app as celery_app

__all__ = ("celery_app",)


VERSION = (2, 3, 0, "final", 0)

__version__ = get_version(VERSION)
