from rest_framework import viewsets
from app.models import Station, Photo, Service, Fuel
from app.serializers import StationSerializer, PhotoSerializer, ServiceSerializer, FuelSerializer
from app.pagination import DynamicPageSizePagination


class PhotoiewSet(viewsets.ModelViewSet):
    queryset = Photo.objects.all()
    serializer_class = PhotoSerializer
    pagination_class = DynamicPageSizePagination


class ServiceViewSet(viewsets.ModelViewSet):
    queryset = Service.objects.all()
    serializer_class = ServiceSerializer
    pagination_class = DynamicPageSizePagination


class FuelViewSet(viewsets.ModelViewSet):
    queryset = Fuel.objects.all()
    serializer_class = FuelSerializer
    pagination_class = DynamicPageSizePagination


class StationViewSet(viewsets.ModelViewSet):
    queryset = Station.objects.all()
    serializer_class = StationSerializer
    pagination_class = DynamicPageSizePagination

