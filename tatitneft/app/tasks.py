from celery import shared_task
import json
from django.contrib.gis.geos import Point
from app.models import Station, Photo, Service
from app.serializers import ServiceSerializer, PhotoSerializer, FuelSerializer
from django.db import transaction
from app.transaction_lock import pg_try_advisory_xact_lock, PglockType


@shared_task(name="app:get_data_from_source_1")
def do_load_data_1():
    with transaction.atomic():
        get_data_from_source_1()


@shared_task(name="app:get_data_from_source_2")
def do_load_data_2():
    with transaction.atomic():
        get_data_from_source_2()


def get_data_from_source_1():
    if not pg_try_advisory_xact_lock(PglockType.DATA):
        return None

    # offset = 0
    # step = 100
    # source_api_url = f"https://source.api.ru/"
    #
    # data = []
    # while True:
    #     with requests.get(
    #         url=source_api_url,
    #             params={"limit": step, "offset": offset},
    #         auth=HTTPBasicAuth("admin_login", 'admin_pass'),
    #     ) as source_data:
    #         if source_data.status_code == 200:
    #             data += source_data.json().get("results")
    #         else:
    #             break
    #     offset += step

    with open('test_sources/source_1.json', encoding='UTF-8') as read_file:
        data = json.load(read_file)

    for station_id, station_data in data.items():
        if not station_data:
            continue

        station, created = Station.objects.get_or_create(
            external_identifier=station_id
        )
        station.coordinates = Point(list(map(
            float,
            station_data.get('coordinates', '0 0').split()
        )))
        station.number = station_data.get('number', None)
        station.address = station_data.get('address', None)
        if not station:
            continue

        if not created:
            station.services.all().delete()
            station.photos.all().delete()

        photos = []
        for photo in station_data.get('photos', []):
            photo_serializer = PhotoSerializer(
                data={
                    'title': f"Изображение станции номер: {station.number}",
                    'url': photo,
                    'stations': station
                }
            )
            if photo_serializer.is_valid():
                photos.append(Photo(**photo_serializer.data))
        station.photos.set(Photo.objects.bulk_create(photos))
        services = []
        for service in station_data.get('services'):
            service_raw_data = list(map(str.strip, service.split("-")))

            if len(service_raw_data) == 2:
                # service_serializer = ServiceSerializer(
                #     data={
                #         'title': service_raw_data[0],
                #         'amount': service_raw_data[1],
                #         'station': station.id
                #     }
                # )
                #
                # print(service_serializer.is_valid(), service_serializer.errors, service_serializer.data)
                # jopa = service_serializer.data
                # jopa.
                # if service_serializer.is_valid():
                services.append(
                    Service(
                        title=service_raw_data[0],
                        amount=service_raw_data[1],
                        station=station
                    )
                )
        Service.objects.bulk_create(services)
        station.save()

def get_data_from_source_2():
    if not pg_try_advisory_xact_lock(PglockType.DATA):
        return None
    # offset = 0
    # step = 100
    # source_api_url = f"https://source.ap2.ru/"
    #
    # data = []
    # while True:
    #     with requests.get(
    #         url=source_api_url,
    #             params={"limit": step, "offset": offset},
    #         auth=HTTPBasicAuth("admin_login", 'admin_pass'),
    #     ) as source_data:
    #         if source_data.status_code == 200:
    #             data += source_data.json().get("results")
    #         else:
    #             break
    #     offset += step

    with open('test_sources/source_2.json', encoding='UTF-8') as read_file:
        data = json.load(read_file)

    for station_id, fuel_data in data.items():
        if not fuel_data:
            continue
        if station := Station.objects.prefetch_related(
                'services', 'photos'
        ).filter(external_identifier=station_id).first():
            for fuel_kind, fuel_kind_data in fuel_data.items():

                title = fuel_kind
                amount = fuel_kind_data[0]
                currency = fuel_kind_data[1]

                fuel_serializer = FuelSerializer(
                    data={
                        "title": title,
                        "amount": amount,
                        "currency": currency,
                        "station": station.id,
                    }
                )
                if fuel_serializer.is_valid():
                    fuel_serializer.save()

        print(station_id)
