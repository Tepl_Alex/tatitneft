from rest_framework import serializers
from rest_framework.relations import StringRelatedField

from app.models import Station, Photo, Service, Fuel


class PhotoSerializer(serializers.ModelSerializer):

    class Meta:
        model = Photo
        fields = "__all__"


class ServiceSerializer(serializers.ModelSerializer):
    station = StringRelatedField()
    # StationSerializer(
    #     required=False,
    #     allow_null=True,
    # )
    photos = PhotoSerializer(
        many=True,
        required=False,
        allow_null=True,
    )

    class Meta:
        model = Service
        fields = "__all__"


class FuelSerializer(serializers.ModelSerializer):
    photos = PhotoSerializer(
        many=True,
        required=False,
        allow_null=True,
    )
    class Meta:
        model = Fuel
        fields = "__all__"


class StationSerializer(serializers.ModelSerializer):
    photos = PhotoSerializer(
        many=True,
        required=False,
        allow_null=True,
    )
    services = ServiceSerializer(
        many=True,
        required=False,
        allow_null=True,
    )
    fuels = FuelSerializer(
        many=True,
        required=False,
        allow_null=True,
    )
    class Meta:
        model = Station
        fields = "__all__"


