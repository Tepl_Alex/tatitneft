from django.contrib import admin
from app.models import Photo, Service, Fuel, Station

admin.site.register(Photo)
admin.site.register(Service)
admin.site.register(Fuel)
admin.site.register(Station)
# Register your models here.
