from rest_framework.routers import DefaultRouter
from app.views import StationViewSet, PhotoiewSet, ServiceViewSet, FuelViewSet

urlpatterns = []

router = DefaultRouter()

router.register(r"photo", PhotoiewSet, basename="photos")
router.register(r"service", ServiceViewSet, basename="services")
router.register(r"fuel", FuelViewSet, basename="services")
router.register(r"station", StationViewSet, basename="stations")

urlpatterns += router.urls
