from enum import Enum, auto
from django.db import transaction


class PglockType(Enum):
    DATA = auto()


def pg_try_advisory_xact_lock(key):
    conn = transaction.get_connection()
    if not conn.in_atomic_block:
        raise RuntimeError(
            "Нельзя использовать pg_try_advisory_xact_lock вне транзакции"
        )
    cursor = conn.cursor()
    command = f"SELECT pg_try_advisory_xact_lock({key.value});"
    cursor.execute(command)
    (result,) = cursor.fetchone()
    return result