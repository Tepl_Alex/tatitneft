from django.contrib.gis.db import models
from django.core.validators import MaxLengthValidator, MinLengthValidator
from django.contrib.gis.geos import Point
from django.db.models import CASCADE


class Currency(models.TextChoices):
    """
    Валюта
    """

    RUB = "Российский рубль"
    USD = "Доллар США"
    EUR = "Евро"


class Photo(models.Model):
    title = models.CharField(
        max_length=250,
        null=True,
        validators=[MinLengthValidator(2), MaxLengthValidator(250)],
        verbose_name="Название изображения",
    )
    url = models.CharField(
        max_length=500,
        verbose_name="URL изображения"
    )
    # class Meta:
    #     ordering = ["-id"]

    def __str__(self):
        return f"Изображение: {self.title}"


class Station(models.Model):
    external_identifier = models.IntegerField(
        null=True,
        verbose_name="внешний идентификатор"
    )
    coordinates = models.PointField(
        srid=4326,
        null=True,
        default=Point(x=49.1114975, y=55.7943584),
        verbose_name="Область карты"
    )
    number = models.IntegerField(verbose_name="Номер", null=True)
    address = models.CharField(
        max_length=255,
        null=True,
        validators=[MinLengthValidator(5), MaxLengthValidator(250)],
        verbose_name="Местоположение",
    )
    photos = models.ManyToManyField(
        Photo,
        verbose_name="Изображения",
        related_name="stations",
        null=True,
    )
    # class Meta:
    #     ordering = ["-id"]

    def __str__(self):
        return f"Станция номер: {self.number}"


class Service(models.Model):
    title = models.CharField(
        max_length=250,
        null=True,
        validators=[MinLengthValidator(5), MaxLengthValidator(250)],
        verbose_name="Название услуги",
    )
    photos = models.ManyToManyField(
        Photo,
        verbose_name="Изображения",
        related_name="services",
        null=True,

    )
    amount = models.DecimalField(
        max_digits=12,
        decimal_places=2,
        verbose_name="Цена услуги"
    )
    currency = models.CharField(
        max_length=255,
        choices=Currency.choices,
        verbose_name="Валюта",
        default=Currency.RUB
    )
    station = models.ForeignKey(
        Station,
        verbose_name="Станция",
        related_name="services",
        on_delete=CASCADE,
        null=True,
    )
    # class Meta:
    #     ordering = ["-id"]

    def __str__(self):
        return f"Услуга: {self.title}"


class Fuel(models.Model):
    title = models.CharField(
        max_length=250,
        null=True,
        validators=[MinLengthValidator(1), MaxLengthValidator(250)],
        verbose_name="Название топлива",
    )
    amount = models.DecimalField(
        max_digits=12,
        decimal_places=2,
        verbose_name="Цена топлива"
    )
    currency = models.CharField(
        max_length=255,
        choices=Currency.choices,
        verbose_name="Валюта",
        default=Currency.RUB
    )
    photos = models.ManyToManyField(
        Photo,
        verbose_name="Изображения",
        related_name="fuels",
        null=True,
    )
    station = models.ForeignKey(
        Station,
        verbose_name="Станция",
        related_name="fuels",
        on_delete=CASCADE,
        null=True,
    )
    # class Meta:
    #     ordering = ["-id"]

    def __str__(self):
        return f"Топливо: {self.title}"


