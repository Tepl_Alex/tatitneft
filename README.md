# TatItNeft

## Зависимости

* postgis/postgis:12-3.0
* rabbitmq:3.8
* redis:6



## Настройка приложения

### Настройка через переменные окружения

* Является основным способом конфигурации приложения.
* Для автоматической загрузки переменных нужно создать `.env` файл, где переменные объявляются как пары `ключ=значение`.
* 

### settings.py

### Минимальная конфигурация (.env)

```buildoutcfg
DATABASES_NAME="test"
DATABASES_USER="test"
DATABASES_PASSWORD="test"
DATABASES_HOST="postgresql"
DATABASES_PORT="5432"
REDIS_LOCATION="redis://redis:6379/1"
REDIS_KEY_PREFIX="test"
CELERY_BROKER_URL="amqp://jobs:jobs_test@rabbitmq:5672/jobs"
WS_BROKER_URL="amqp://ws:ws_test@rabbitmq:5672/ws"
```


## Настройка PostgreSQL

```sql
CREATE EXTENSION postgis;
```


## Настройка RabbitMQ

> bash rabbitmq/scripts/init_rabbitmq.sh


## Схема API

> host_name:port/swagger/

## Общая схема работы по загрузке данных из источников

* Посредством библиотеки Celery реализован запуск задач по загрузке данных в определенное время
* Настройки времени указываются в настройках проекта (settings.py)
* Взаимодействие с клиентами реализовано посредством DRF
* Для управления библиотеками используется Pipenv
* Для продуктового запуска используется Docker/Docker-compose 
* Изменение данных может производиться с использованием админпанели:
    > host_name:port/admin/
